const Discord = require('discord.js');
const client = new Discord.Client();
const cfg = require('./config.json');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {

  if (msg.content.indexOf(`<@${client.user.id}>`) !== -1 && msg.guild) {
    //msg.reply('!');
    if (!msg.author.bot) {
      msg.guild.fetchMembers()
        .then((g) => {
          let members = g.members.filter((b) => !b.user.bot).array();
          let i = Math.floor(Math.random() * (members.length));
          console.log(i);
          msg.channel.send(`<@${members[i].id}>`);
        })
        .catch(console.error);
    }
  }
});

client.login(cfg.token);
